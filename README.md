# Python-FMP6

I wrote this a long time ago to help convert a bunch of old [FileMaker
Pro](http://en.wikipedia.org/wiki/FileMaker) version 6 databases to
[MongoDB](http://mongodb.org/).

It was never intended to work with earlier or later versions of FMP. It also
requires the no longer developed [appscript](http://appscript.sourceforge.net)
python-AppleScript library.

I no longer have a means to run FMP v6 (that was PowerPC code, and the emulator
for that has been removed from OS X) so I can't update or test this, but it did
get the job done for a database of 38,000 job candidates, around 6,000
employers, and a bunch of other stuff (jobs, etc.).

So I'm just putting this up here in case it can be of any use to anyone.
Probably it isn't. Whatevs.

