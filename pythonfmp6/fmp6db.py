#!/usr/bin/python2.6
# -*- coding: utf-8 -*-

from pprint import pprint, pformat
import logging
import re
import os
import datetime
from appscript import *
from dateutil.parser import parse as du_parse

log = logging.getLogger(__name__)

"""
######
FMP6DB
######

.. note: Currently, this library assumes that you have the file(s) you want to access 
         already open in FMP6, and that a layout exists which displays all the fields you will need to access. 
         Opening files on an FMP server via appscript has so far proven to be likely impossible.

.. note: This will only talk to FileMaker Pro v6.0v4. If you have any newer versions installed, appscript will 
         try to open the newer version no matter what you give it for `fmp_app_name` and then it all goes 
         downhill from there.

Arguments
=========

``db_name`` is the name of the database. It is sometimes case-sensitive and sometimes not,
so provide a value that assumes it always is.

``layout_name`` is the name of an FMP layout which has all the fields you will need to access.


``field_spec``
    A tuple of tuples that helps automatically translate and convert the field names. 
    There are a few special ones included: 
    
    * ``'date'``, ``'time'``: obvious.
    * ``'checkboxes'`` - which converts the value to a tuple of unicode strings, one for each checked value.
    * ``'repeating long'`` - converts to a list of longs
    * ``'repeating int'`` - converts to a list of ints
    * ``'repeating date'`` - converts to a list of dates
    
    .. note:: repeating text fields are automatically converted to a list of Unicode strings

    All field values are automatically converted from MacRoman to Unicode, and line breaks from
    old-school Mac OS 9 style (\r) to Unix style (\n). Note that there is no known way to extract
    styled text from FMP6 short of copying and pasting, and even that is flaky at best.

    .. note:: This library currently does not handle media fields (image, audio, video).

    Example::
    
        (
            ('old_name', 'new_name', conversion_function),
            # examples
            ('Person First Name', 'name_first', unicode),
            ('RecordID', 'id', long),
            ('Come On FHQWHGADS', 'fhqwhgads', lambda x: unicode.lower(unicode(x))),
        )
    
    If you say ``None`` for ``new_name`` in the tuple, an attempt will be made to create a Pythonic
    name: all lowercase, and spaces to underscores.

``datetimes``
    A tuple of tuples - each triplet is the FMP name of a date field, a time field, and whatever the field name should become after processing. They'll be combined into a proper Python datetime object and the originals removed. 
    Example::
    
        (
            ('created_date', 'created_time', 'created'),
        )

``pre_processor`` is a callable which will be a passed a dictionary of the raw data, prior to renaming or type conversion. It should modify the dictionary in-place, but not remove any keys that are in ``field_spec``.

``post_processor`` is a callable which will be passed the raw data (after your ``pre_processor``, if supplied, has been run) and the final data, after renaming fields and doing type conversions. Modify the final dictionary in-place.

``auto_id``: If True, and there is no field in the processed data
    named 'id', then it will create one and assign a sequential value to it. Default: True. 

``starting_id``: default: 1, a starting number to use with auto_id

"""





"""
after doing a find you have to refer to documents[1] to work with results, which means the window has to be in front

the normal window.show() does not work as expected. you have to say documents[name].current_record.get().go_to()

"""

__all__ = ['FMP6DB']


FMP_APP_NAME = u'FileMaker Pro.app'

line = '-'*70

ILLEGAL_CHARS = "!@#$%^&(){}[]-/\\|,.`~'\""

TOO_MANY_UNDERSCORES = re.compile(r'_{2,}')



def is_datetime(d):
    if isinstance(d, datetime.datetime) and getattr(d, 'second', False):
        return True
    return False

def is_date(d):
    if isinstance(d, datetime.date) and not getattr(d, 'second', False):
        return True
    return False

def is_time(d):
    if isinstance(d, datetime.time) and not getattr(d, 'month', False):
        return True
    return False


def whatdate(d):
    """
    Returns 'datetime', 'date', 'time', or `None` if none of the above.
    """
    if is_datetime(d):
        return 'datetime'
    if is_date(d):
        return 'date'
    if is_time(d):
        return 'time'
    return None


def parse(ds):
    dt = None
    try:
        dt = du_parse(ds)
    except ValueError, msg:
        print "WARNING: date/time string {0} could not be parsed.".format(unicode(ds).encode('ascii', 'replace'))
    if is_time(dt):
        return dt
    return dt
    


def deep_set_dict(adict, key, value):
    if not "." in key:
        adict[key] = value
        return
    segment = key.split('.')[0]
    subdict = adict.get(segment, dict())
    next_key = key.replace(segment + '.', '')
    deep_set_dict(subdict, next_key, value)
    adict[segment] = subdict


class deepdict(dict):
    def __setitem__(self, key, value):
        if not '.' in key:
            super(deepdict, self).__setitem__(key, value)
            return
        segment = key.split('.')[0]
        subdict = self.get(segment, None)
        if not subdict:
            subdict = deepdict()
        next_key = key.replace(segment + '.', '')
        subdict[next_key] = value
        super(deepdict, self).__setitem__(segment, subdict)

    def __getitem__(self, key):
        if not '.' in key:
            return super(deepdict, self).__getitem__(key)
        segment = key.split('.')[0]
        subdict = super(deepdict, self).__getitem__(segment)
        if not isinstance(subdict, dict):
            return subdict
        next_key = key.replace(segment + '.', '')
        return subdict.__getitem__(next_key)



def test_deepdict():
    d1 = deepdict()
    d1['hello.monkey'] = 4
    assert d1['hello']['monkey'] == 4
    d1['hello.ostrich'] = 5
    assert d1['hello']['ostrich'] == 5
    d1['hello.bears.grizzly'] = 4
    assert d1['hello.bears.grizzly'] == 4
    assert d1['hello']['bears']['grizzly'] == 4
    print d1



class Counter(object):
    def __init__(self, start=None):
        if not start:
            start = 1
        self.value = start
    def new(self):
        self.value += 1
        return self.value



class FMP6DB(object):
    
    def __init__(self, db_name, layout_name, field_spec, datetimes=None, fmp_app_name=None, pre_processor=None, post_processor=None, auto_id=None, starting_id=None, password=None, quiet=True, extras=None):
        global FMP_APP_NAME
        self.fmp_app_name = fmp_app_name
        if not self.fmp_app_name:
            self.fmp_app_name = FMP_APP_NAME
        self.datetimes = datetimes
        if not datetimes:
            self.datetimes = list()
        self.db_name = db_name
        self.layout_name = layout_name
        self.field_spec = field_spec
        self.pre_processor = pre_processor
        self.post_processor = post_processor
        self._been_switched_to = False
        self.password = password if password else u''
        if auto_id is None:
            auto_id = True
        self.auto_id = auto_id
        self._counter = Counter(starting_id)
        self._by_id = dict()
        self.quiet = quiet
        self.open_documents = list()
        if extras:
            self.extras = extras
        else:
            self.extras = dict()
    
    def open_document(self, force=False):
        """
        Open my document if it is not already.
        """
        fmp_version = app(self.fmp_app_name).version.get()
        if fmp_version != '6.0v4':
            raise Exception, "{0} is the wrong version of FileMaker! I only talk to 6.0v4".format(fmp_version)
        self.list_open_documents()
        if self.db_name not in self.open_documents:
            force = True
        if not force:
            return
        print u"Opening {0}".format(self.db_name)
        if self.password:
            cmdt = u"""osascript -e 'with timeout of 60 * 5 seconds' -e 'tell application "{app_name}" to open file "{file_name}" with password "{password}"' -e 'end' """
        else:
            cmdt = u"""osascript -e 'with timeout of 60 * 5 seconds' -e 'tell application "{app_name}" to open file "{file_name}"' -e 'end' """
        cmd = cmdt.format(app_name=self.fmp_app_name, file_name=self.db_name, password=self.password)
        # print cmd
        os.system(cmd)
        
    
    
    def list_open_documents(self, refresh=False):
        """
        Return a list of open documents.
        """
        if not self.open_documents:
            refresh = True
        if not refresh:
            return self.open_documents
        count = app(self.fmp_app_name).count(None, class_=k.document)
        documents = list()
        for i in xrange(1, count):
            documents.append(app(self.fmp_app_name).documents[i].name.get_data())
        self.open_documents = documents
        return documents
    
    @property
    def db(self):
        return app(self.fmp_app_name).databases[self.db_name]
    
    @property
    def doc(self):
        return app(self.fmp_app_name).documents[self.db_name]
    
    @property
    def app(self):
        return app(self.fmp_app_name)
    
    @property
    def layout(self):
        return app(self.fmp_app_name).documents[self.db_name].layouts[self.layout_name]
    
    def show(self):
        """
        Bring the database's window to the front.
        """
        if not self._been_switched_to:
            self.switch_to()
        self.doc.current_record.get().go_to()
    
    def switch_to(self, other_layout=None):
        """
        Brings the window to the front and switches to layout. window.show() does not work 
        under Snow Leopard. If you want to use a different layout than the one specified at 
        instantiation, use `other_layout`.
        """
        layout_name = self.layout_name
        if other_layout:
            layout_name = other_layout
        self.doc.layouts[layout_name].go_to()
        self._been_switched_to = True
    
    
    def first_record(self):
        """Goes to the first record of the current found set, returns True if successful,
        False if not."""
        # if not self._been_switched_to:
        #     self.switch_to()
        try:
            self.layout.records[1].go_to()
            return True
        except Exception, msg:
            if not self.quiet:
                print msg
            return False
    
    
    def next_record(self):
        """
        Tries to go to the next record in the current found set. Returns True if it 
        worked, False if it failed (a good way to know you've hit the end of the found set and should
        break out of your loop).
        """
        # if not self._been_switched_to:
        #     self.switch_to()
        try:
            self.layout.current_record.next(k.record_).go_to()
            return True
        except Exception, msg:
            if not self.quiet:
                print msg
            return False
    
    
    def go_to_record(self, number):
        """
        Tries to go to the next record in the current found set. Returns True if it 
        worked, False if it failed (a good way to know you've hit the end of the found set and should
        break out of your loop).
        """
        # if not self._been_switched_to:
        #     self.switch_to()
        try:
            self.layout.records[number].go_to()
            return True
        except Exception, msg:
            if not self.quiet:
                print msg
            return False
    

    def show_all_records(self):
        self.db.records.show()
    
    @staticmethod
    def _minimal_clean_string(s, mac_decode=True):
        """Decodes MacRoman to Unicode, converts CR to LF, and strips."""
        if mac_decode:
            s = s.decode('MacRoman')
        s = s.strip().replace('\r', '\n')
        return s
        
    
    def get_cell_data(self, cell_name):
        if not self._been_switched_to:
            self.switch_to()
        try:
            val = self.doc.current_record.cells[cell_name].get_data()
            if isinstance(val, basestring):
                val = self._minimal_clean_string(val, True)
                return val
            if isinstance(val, list):
                new_list = list()
                for item in val:
                    if isinstance(item, basestring):
                        item = self._minimal_clean_string(item, True)
                    if len(item) == 0:
                        continue
                    new_list.append(item)
                return new_list
        except Exception, msg:
            if not self.quiet:
                print msg
            return None
    
    @staticmethod
    def _pythonic_name(s):
        s = s.strip().replace(' ', '_').replace('.', '_').lower()
        for c in ILLEGAL_CHARS:
            if c in s:
                s = s.replace(c, '_')
        s = re.sub(TOO_MANY_UNDERSCORES, '_', s)
        return s
    
    
    def get_raw_data(self):
        raw_data = dict()
        for name_set in self.field_spec:
            raw_data[name_set[0]] = self.get_cell_data(name_set[0])
        for d, t, name in self.datetimes:
            raw_data[d] = self.get_cell_data(d)
            raw_data[t] = self.get_cell_data(t)
        return raw_data
    
    
    
    def process_raw_data(self, raw_data, old_name_processor=None):
        """
        If provided, old_name_processor will be called on the raw data's key names before attempting to access them.
        """
        data = deepdict()
        for d, t, name in self.datetimes:
            if raw_data.get(d, None) and raw_data.get(t, None):
                data[name] = parse(raw_data[d] + ' ' + raw_data[t])
            else:
                data[name] = None
        if callable(self.pre_processor):
            self.pre_processor(raw_data)
        for old_name, new_name, converter in self.field_spec:
            if callable(old_name_processor):
                orig_name = old_name
                old_name = old_name_processor(old_name)
                # log.debug("old_name_processor changed {0} to {1}".format(orig_name, old_name))
            if old_name not in raw_data:
                # log.debug("Key name {0} not found in the raw data...".format(old_name))
                continue
            key = new_name
            if not key:
                key = self._pythonic_name(old_name)
            data[key] = raw_data[old_name]
            if not isinstance(converter, (tuple, list)):
                converters = [converter]
            else:
                converters = converter
            for converter in converters:
                if isinstance(converter, basestring):
                    if converter in ('date', 'time', 'datetime'):
                        if not isinstance(data[key], basestring):
                            data[key] = str(data[key])
                        if data[key]:
                            data[key] = parse(data[key])
                        else:
                            data[key] = None
                        if isinstance(data[key], datetime.datetime) and hasattr(data[key], 'second') and converter in ('date', 'time'):
                            if converter == 'date':
                                data[key] = data[key].date()
                            elif converter == 'time':
                                data[key] = data[key].time()
                        if converter == 'datetime' and not isinstance(data[key], datetime.datetime):
                            if isinstance(data[key], datetime.time):
                                raise ValueError("Cannot convert a time to a datetime. That makes no sense.")
                            elif isinstance(data[key], datetime.date):
                                nd = datetime.datetime(data[key].year, data[key].month, data[key].day, 
                                                       0, 0, 0, 0)
                                data[key] = nd
                        if isinstance(data[key], datetime.datetime):
                            od = data[key]
                            nd = datetime.datetime(od.year, od.month, od.day, od.hour or 0, 
                                                   od.minute or 0, od.second or 0, od.microsecond or 0)
                            data[key] = nd
                        continue
                    if converter == 'strip':
                        if not isinstance(data[key], basestring):
                            continue
                        data[key] = data[key].strip()
                        continue
                    if converter == 'checkbox':
                        print "Oh no, I don't know how to handle checkboxen yet."
                        pprint(data[key])
                    else:
                        raise ValueError("I don't have a converter named {0}".format(converter))
                elif callable(converter) and converter is not bool:
                    try:
                        if len(data[key]) > 0:
                            data[key] = converter(data[key])
                    except Exception, msg:
                        if not self.quiet:
                            print "Your converter could not run on field/data: {0}:{1}".format(key, data[key])
                            print msg
                elif converter is bool:
                    if not data[key]:
                        data[key] = False
                    if isinstance(data[key], basestring):
                        value = data[key].lower().strip()
                        if value in [u'1', u'x', u'true', u'√', u'•', u'*', u'yes']:
                            data[key] = True
                        else:
                            data[key] = False
                    if isinstance(data[key], (int, long, float)):
                        if data[key] >= 1:
                            data[key] = True
                        else:
                            data[key] = False
        if 'id' not in data and self.auto_id:
            data['id'] = self._counter.new()
        if callable(self.post_processor):
            self.post_processor(raw_data, data)
        return data
    
    def import_row(self):
        """get raw values from the current record FMP and convert them. 
           returns a dict with converted fields"""
        raw_data = self.get_raw_data()
        data = self.process_raw_data(raw_data)
        return data
    
    def import_current_found_set(self, limit=None, offset=None):
        """Returns a list of dicts of converted data."""
        count = 0
        if limit:
            self.go_to_record(offset)
        else:
            self.first_record()
        records = list()
        more_records = True
        while more_records:
            records.append(self.import_row())
            more_records = self.next_record()
            count += 1
            if limit and count > limit:
                more_records = False
        return records
    
    def list_fields(self, sort=True):
        """List of fields on current layout."""
        if not self._been_switched_to:
            self.switch_to()
        fields = self.db.fields.name.get_data()
        if sort:
            fields.sort()
        return fields
    
    @staticmethod
    def list_open_databases(app_name=None):
        if app_name is None:
            app_name = FMP_APP_NAME
        docnames = list()
        more = True
        docindex = 1
        while more:
            try:
                name = app(app_name).documents[docindex].name.get()
                docnames.append(name)
            except:
                more = False
            docindex += 1
        return docnames


