
FMP_APP_NAME = u'/Applications/FileMaker Pro.app'

def test():
    from pythonfmp6 import FMP6DB
    contacts = FMP6DB(
        "Contacts_.fp5",
        "all",
        (
            ('id', None, long),
            ('Contact First', 'name_first', None),
            ('Contact Last', 'name_last', None),
            ('Contact Title', 'title', None),
            ('Contact Phone', 'phone', None),
            ('Phone Extension', 'phone_ext', None),
            ('Chair', 'chair', None),
            ('Full Name', 'useless_box', None),
            ('Practice Area', 'practice_area', None),
        ),
        fmp_app_name = FMP_APP_NAME
        
    )
    return contacts