from setuptools import setup, find_packages
import sys, os

version = '0.1.1'

setup(name='python-fmp6',
      version=version,
      description="Helpful library for accessing FileMaker Pro 6 from Python on Snow Leopard.",
      long_description="""\
""",
      classifiers=[], # Get strings from http://pypi.python.org/pypi?%3Aaction=list_classifiers
      keywords='filemaker mac snowleopard fmp fmp6 database',
      author='Isaac Csandl',
      author_email='isaac.csandl@me.com',
      url='http://nerkles.com/',
      license='GPL',
      packages=find_packages(exclude=['ez_setup', 'examples', 'tests']),
      include_package_data=True,
      zip_safe=False,
      install_requires=[
          # -*- Extra requirements: -*-
          'appscript',
          'python-dateutil',
      ],
      entry_points="""
      # -*- Entry points: -*-
      """,
      )
